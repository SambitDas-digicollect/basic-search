//
//  ViewController.swift
//  Basic Search
//
//  Created by Sambit Das on 11/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    
    
    @IBOutlet weak var StatusLbl: UILabel!
    @IBOutlet weak var RecrutingRadioBttn: UIButton!
    @IBOutlet weak var AllStudsRadioBttn: UIButton!
    @IBOutlet weak var conditionTextfield: underLineTextfield!
    @IBOutlet weak var otherTermTextField: underLineTextfield!
    @IBOutlet weak var countryTextfield: underLineTextfield!
    @IBOutlet weak var searchButton: UIButton!
    
    
    
    var CollectionViewButton = [" Conditions "," Rare Disease "," Drug Interventions "," Sponser / Collaboration "," Diatary Supplements "]
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    
        
    
        //rightView for condition textfield
        conditionTextfield.rightView = UIImageView(image: UIImage(systemName: "exclamationmark.octagon.fill"))
        conditionTextfield.rightView?.tintColor = .white
        conditionTextfield.rightView?.frame = CGRect(x: 0, y: 5, width: 20 , height:20)
        conditionTextfield.rightViewMode = .always
        
        //rightView for OtherTerm Textfield
        otherTermTextField.rightView = UIImageView(image: UIImage(systemName: "exclamationmark.octagon.fill"))
        otherTermTextField.rightView?.tintColor = .white
        otherTermTextField.rightView?.frame = CGRect(x: 0, y: 5, width: 20 , height:20)
        otherTermTextField.rightViewMode = .always
        
        //rightView for Country Textfield
        countryTextfield.rightView = UIImageView(image: UIImage(systemName: "exclamationmark.octagon.fill"))
        countryTextfield.rightView?.tintColor = .white
        countryTextfield.rightView?.frame = CGRect(x: 0, y: 5, width: 20 , height:20)
        countryTextfield.rightViewMode = .always
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        searchButton.layer.cornerRadius = 10
        
        
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 5
        
    }
 
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        cell.CellButton.setTitle(self.CollectionViewButton[indexPath.row], for: .normal)
        cell.CellButton.layer.cornerRadius = 10
        return cell
    }
    
    
   
    @IBAction func RecrutingOptn(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            RecrutingRadioBttn.isSelected = false
            AllStudsRadioBttn.isSelected = true
            
        }else{
           sender.isSelected = true
            RecrutingRadioBttn.isSelected = true
            AllStudsRadioBttn.isSelected = false
            
        }
    }
    
    @IBAction func AllStudiesOption(_ sender: UIButton) {
        if sender.isSelected{
                   sender.isSelected = false
                   AllStudsRadioBttn.isSelected = false
                   RecrutingRadioBttn.isSelected = true
                   
               }else{
                  sender.isSelected = true
                   AllStudsRadioBttn.isSelected = true
                   RecrutingRadioBttn.isSelected = false
               }
        
        
    }
    

    
    
    
    
    @IBAction func searchBttnTapped(_ sender: Any) {
    }
    
    @IBAction func advancedSearchBttnTapped(_ sender: Any) {
    }
    
}
    
    class underLineTextfield : UITextField{
        
    override func  awakeFromNib() {
        super.awakeFromNib()
        
        self.borderStyle = .none
       
        let btmBorder = UIView(frame: .zero)
                          btmBorder.backgroundColor = .white
                          self.addSubview(btmBorder)
                   btmBorder.translatesAutoresizingMaskIntoConstraints = false
                          btmBorder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
                          btmBorder.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
                          btmBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
                          btmBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
    
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.layer.masksToBounds = true
        
    }

}


class underLineButton : UIButton{
    
    override func  awakeFromNib() {
        super.awakeFromNib()
        
        let btmBorder = CALayer()
        let borderWidth : CGFloat = 2
        btmBorder.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btmBorder.borderWidth = borderWidth
        btmBorder.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: 2)
        self.layer.addSublayer(btmBorder)
        //self.borderStyle = .none
        self.backgroundColor = UIColor.clear
        self.layer.masksToBounds = true
        
    }

}
